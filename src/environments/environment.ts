// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBiVyeSVFI-EHTxB0auRRea7VGRMkU04A4",
    authDomain: "example-c1303.firebaseapp.com",
    databaseURL: "https://example-c1303.firebaseio.com",
    projectId: "example-c1303",
    storageBucket: "example-c1303.appspot.com",
    messagingSenderId: "805215892798",
    appId: "1:805215892798:web:2bfc647025baaa074c286f",
    measurementId: "G-WK6548B5VC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
