import { HttpClient } from '@angular/common/http';
import { Article } from './../interfaces/article';
import { User } from '../interfaces/user.model';
import { Observable } from 'rxjs';
import { AngularFirestore,AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClassificationService {
  
  private url = "https://o60mxx7cql.execute-api.us-east-1.amazonaws.com/beta"
  public categories:object ={0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  public doc :string;
  private usersCollection:AngularFirestoreCollection<User>;//יצירת משתנה מסוג שיאפשר גישה לקולקשיין בתוך הדאטה בייס 
  private users:Observable<User[]>;//get בשביל להשתמש אחרי זה בפונקציות  Item מסוג  Observable יצירת משתנה שיוכל לקבל ערכי 
  private articlesCollection:AngularFirestoreCollection;
  private articles:Observable<Article[]>
  

  constructor(private http:HttpClient,private db:AngularFirestore) {
    this.usersCollection=db.collection<User>('users');//יצרנו משתנה ,המשתנה יאפשר לנו ליצור קשר עם הקולקשיין בדאטאבייס
  } 
  
  classify():Observable<any>{
    let json={
      "articles":[
        {"text":this.doc}
      ]
    }
    let body= JSON.stringify(json);//פעולה שהופכת את התוכן לסטרינג/טקסט
    return  this.http.post<any>(this.url,body).pipe(
      map(res=>{
        console.log(res.body)
        let final = res.body.replace('[','')//במבנה שאני מקבל אני מקבל מערך כלומר יהיה לי סוגריים מרובעות שאותם ארצה להוריד וכך אעשה זאת
        final = final.replace(']','');
        return final;
      })
    )
  }
  addArticle(content:string,category:string,userId:string){
    console.log("i'm added article")
    const articleAdded={content:content,category:category}
   this.usersCollection.doc(userId).collection('articles').add(articleAdded);
  }
  
  //פוקציה שמציגה את כל הערכים הנמצאים בקולקשין הספציפי
   getArticles(userId:string):Observable<any[]>{
    this.articlesCollection=this.db.collection(`users/${userId}/articles`)
    console.log('items collection created');
    return this.articlesCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    );
   }
   //ספציפי שאותו נרצה להציג בתוך טופס העריכה  Item הפונקצה הבאה מטרתה להחזיר ערכי 
   getArticle(userId:string,articleId:string):Observable<any>{
     return this.db.doc(`users/${userId}/articles/${articleId}`).get();
   }
   editArticle(content:string,category:string,articleId:string,userId:string){
    this.db.doc(`users/${userId}/articles/${articleId}`).update({
      content:content,category:category
    })
   }
   //בשביל למחוק ערך מהדאטא בייס
   deleteArticle(articleId:string,userId:string){
     console.log(articleId)
     console.log(userId)
    this.db.doc(`users/${userId}//articles/${articleId}`).delete();
   }
  }
