import { User } from '../interfaces/user.model';
import { Observable } from 'rxjs';
import { AngularFirestore,AngularFirestoreCollection } from '@angular/fire/firestore';
import { Item } from './../interfaces/item';
import { ItemAttr } from './../shared/itemAttr.model';
import { Injectable } from '@angular/core';
import { map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private usersCollection:AngularFirestoreCollection<User>;//יצירת משתנה מסוג שיאפשר גישה לקולקשיין בתוך הדאטה בייס 
  private users:Observable<User[]>;//get בשביל להשתמש אחרי זה בפונקציות  Item מסוג  Observable יצירת משתנה שיוכל לקבל ערכי 
  private itemsCollection:AngularFirestoreCollection;
  private items:Observable<Item[]>
  

constructor(private db:AngularFirestore) { 
  this.usersCollection=db.collection<User>('users');//יצרנו משתנה ,המשתנה יאפשר לנו ליצור קשר עם הקולקשיין בדאטאבייס
  }
addItem(name:string,price:number,userId:string){
  console.log("i'm added item")
  const itemAdded={name:name,price:price}
 this.usersCollection.doc(userId).collection('items').add(itemAdded);
}

//פוקציה שמציגה את כל הערכים הנמצאים בקולקשין הספציפי
 getItems(userId:string):Observable<any[]>{
  this.itemsCollection=this.db.collection(`users/${userId}/items`)
  console.log('items collection created');
  return this.itemsCollection.snapshotChanges().pipe(
    map(actions => actions.map(a => {
      const data = a.payload.doc.data();
      data.id = a.payload.doc.id;
      return { ...data };
    }))
  );
  //  this.items=this.itemsCollection.valueChanges({idField:'id'})
  //  return this.items;
 }
 //ספציפי שאותו נרצה להציג בתוך טופס העריכה  Item הפונקצה הבאה מטרתה להחזיר ערכי 
 getItem(userId:string,itemId:string):Observable<any>{
   return this.db.doc(`users/${userId}/items/${itemId}`).get();
 }
 editItem(name:string,price:number,itemId:string,userId:string){
  this.db.doc(`users/${userId}/items/${itemId}`).update({
    name:name,price:price
  })
 }
 //בשביל למחוק ערך מהדאטא בייס
 deleteItem(itemId:string,userId:string){
   console.log(itemId)
   console.log(userId)
  this.db.doc(`users/${userId}/items/${itemId}`).delete();
 }


// getItems(){
//   return this.items;
// }
// addItems(item:ItemAttr){//פה אני בעצם דוחף את הפריט לתוך המערך הקיים
//   this.itemsfixd.push(item)
// }


}

