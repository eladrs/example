import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router} from "@angular/router";
import { Location } from "@angular/common";
import { AuthService } from './../services/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent  implements OnInit,OnDestroy{
  private userSub: Subscription;
  isAuthenticated= false;

  title: string = 'Login';
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, location: Location, router: Router,private authService:AuthService){
    router.events.subscribe(val => {
      if (location.path() == "/login") {
        this.title = 'Login';
      } else if(location.path()=="/items") {
        this.title = "Items";
      }
    });   
  }
  ngOnInit(){
    this.userSub=this.authService.user.subscribe(user=>{//בשיטה הנ"ל אנו בעצם בודקים האם המשתמש מחובר
      this.isAuthenticated=!user? false : true;
      console.log(!user)
     

    });
  }
  ngOnDestroy(){
    this.userSub.unsubscribe();
  }
  onLogout(){
   this.authService.logout();
  }

}
