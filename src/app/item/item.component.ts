import { AuthService } from './../services/auth.service';
import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { ItemService } from './../services/item.service';
import { Item } from './../interfaces/item';
import {Router,ActivatedRoute} from '@angular/router';
// import { Observable } from 'rxjs';
//import { ItemAttr } from '../shared/itemAttr.model';



@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  nameItem:string;
  priceItem:number;
  item_ID:string;
  userId:string;
  isEdit:boolean=false;
  dynamicTitle:string='Add item';
  dynamicButtonValue:string='upload item';
  //@ViewChild('name_item',{static:false}) nameItemRef: ElementRef;//מקבל את השם שמתקבל באינפוט
  //@ViewChild('price_item',{static:false}) priceItemRef: ElementRef;
  //priceItem="400";//אם נרצה לשים ערך דיפולטיבי
  constructor(private itemService:ItemService,
    private authService:AuthService,
    private router:Router,
    private route:ActivatedRoute) { }

  sendForm(){
    if(this.isEdit){
      this.itemService.editItem(this.nameItem,this.priceItem,this.item_ID,this.userId)
    }
    else{
    this.itemService.addItem(this.nameItem,this.priceItem,this.userId);
    }
   this.router.navigate(['/items']);
   
  }
  ngOnInit() {
    this.item_ID=this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user=>{
        this.userId=user.id;
        if(this.item_ID){
          this.isEdit=true;
          this.dynamicTitle="Edit item";
          this.dynamicButtonValue="save Changes"
          this.itemService.getItem(this.userId,this.item_ID).subscribe(
            item=>{
              this.nameItem=item.data().name;
              this.priceItem=item.data().price
            })
        }
      }
    )
    
  }
editItem(){

}
}
