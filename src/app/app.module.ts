import { environment } from 'src/environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Output } from '@angular/core';
import { RouterModule,Routes}from '@angular/router';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavComponent } from './nav/nav.component';
import { AuthService } from './services/auth.service';
import { GreetingsComponent } from './greetings/greetings.component';


//angular material כל אלו תיקיות שייבאנו בשביל להשתמש בספריות של 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';

//בכדי לייצא נתונים אל השרת ,לשלוח בקשות אל השרת 
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
//בכדי להעלות תכנים לפיירבייס או למחוק אותם 
import { AngularFirestore,AngularFirestoreModule} from '@angular/fire/firestore';
import { AngularFireAuthModule,AngularFireAuth } from '@angular/fire/auth';
import { ItemService } from './services/item.service';
import { ClassificationComponent } from './classification/classification.component';
import { ArticlesComponent } from './articles/articles.component';
import { ViewArticlesComponent } from './view-articles/view-articles.component';



const appRoutes: Routes = [
  { path: 'items', component: ItemsComponent },
  { path: 'login', component: LoginComponent },
  { path: 'item', component: ItemComponent },
  { path: 'item/:id', component:ItemComponent},
  { path: 'classification', component:ClassificationComponent},
  { path: '**', component: NotFoundComponent },
  { path: '',
    redirectTo: '/login',//הערך הדיפולטיבי אם לא הזנתי ניתוב ספציפי
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    ItemComponent,
    LoginComponent,
    NotFoundComponent,
    NavComponent,
    GreetingsComponent,
    ClassificationComponent,
    ArticlesComponent,
    ViewArticlesComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    HttpClientModule,
    
    RouterModule.forRoot(
      appRoutes,// { enableTracing: true } // <-- debugging purposes only
    ), 
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [AngularFirestore,AngularFireAuth,AuthService,ItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
