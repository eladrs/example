import { AuthService } from './../services/auth.service';
import { Observable } from 'rxjs';
import { Item } from './../interfaces/item';
import { HttpClient } from '@angular/common/http';
import { ItemAttr } from './../shared/itemAttr.model';
import { Component, OnInit } from '@angular/core';
import { ItemService } from './../services/item.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  // items:ItemAttr[];
  items$:Observable<Item[]>;
  userId:string;
  panelOpenState = false;


 
  constructor(private itemService:ItemService,public authService:AuthService) { }

  //כעת לא מספיק להציג את הפריטים,אני רוצה להציג את הפריטים של יוזר מסויים
  ngOnInit() {
    this.authService.getUser().subscribe(
      user=>{
        this.userId=user.id;
        this.items$=this.itemService.getItems(this.userId);//מציג את כל הפריטים שיש לי ברשימה
      }
    )
    
  }
  
  deleteItem(itemId:string){
    this.itemService.deleteItem(itemId,this.userId);
  }
  
  // onAddItem(itemAttr:ItemAttr){
  //   this.items.push(itemAttr);
  // }
  //פונקציה שתשמור נתונים שמוזנים בטופס על השרת
  // onCreateItem(postData:{name:string;price:number}){
  //   this.http.post()

  }
  

