import { ClassificationService } from './../services/classification.service';
import { Article } from './../interfaces/article';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-view-articles',
  templateUrl: './view-articles.component.html',
  styleUrls: ['./view-articles.component.css']
})
export class ViewArticlesComponent implements OnInit {

  articles$:Observable<Article[]>;
  userId:string;

  constructor(private classificationService:ClassificationService,public authService:AuthService) { }

  //כעת לא מספיק להציג את הפריטים,אני רוצה להציג את הפריטים של יוזר מסויים
  ngOnInit() {
    this.authService.getUser().subscribe(
      user=>{
        this.userId=user.id;
        this.articles$=this.classificationService.getArticles(this.userId);//מציג את כל הפריטים שיש לי ברשימה
      }
    ) 
  }
  deleteArticle(articleId:string){
    this.classificationService. deleteArticle(articleId,this.userId);
  }
  
  }
  

