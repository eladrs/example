import { AuthService } from './../services/auth.service';
import { RouterLink,Router, ActivatedRoute } from '@angular/router';
import { ImageService } from '../services/image.service';
import { ClassificationService } from './../services/classification.service';
import { Component, OnInit } from '@angular/core';




@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string;
  content:string;
  userId:string;
  article_ID:string;
  isEdit:boolean=false;
  dynamicTitle:string='Add Article';
  dynamicButtonValue:string='upload Article';

  constructor(public classificationService:ClassificationService,
              public imageService:ImageService,
              private authService:AuthService,
              private router:Router,
              private route:ActivatedRoute) {}
              
         

  //רק בשלב הזה אנחנו בעצם מפעילים את הפונקציה ששולחת את המאמר לצורך הסיווג 
  ngOnInit() {
    this.classificationService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classificationService.categories[res])
        this.category = this.classificationService.categories[res];//פה אני בעצם מבקש לקבל את הסיווג דרך קובץ הסרוויס
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classificationService.doc)
      }
    )
    this.article_ID=this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user=>{
        this.userId=user.id;
        if(this.article_ID){
          this.isEdit=true;
          this.dynamicTitle="Edit article";
          this.dynamicButtonValue="save Changes"
          this.classificationService.getArticle(this.userId,this.article_ID).subscribe(
            article=>{
              this.content=article.data().name;
              this.category=article.data().price
            })
        }
      }
    )
  this.content=this.classificationService.doc;
  }
  saveClassification(){
    if(this.isEdit){
      this.classificationService.editArticle(this.content,this.category,this.article_ID,this.userId)
    }
    else{
    this.classificationService.addArticle(this.content,this.category,this.userId);
    }
    //עדיין לא יצרתי את הקישור הזה ואת הקומפננטה הזאת שמראה את המאמרים 
   this.router.navigate(['/viewArticles']);
   
  }
}

